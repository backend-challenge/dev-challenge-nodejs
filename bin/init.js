import * as app from '../app.js';
import createInitialData from '../lib/utils/generator/generator.js';

export default app.connectMongoose()
  .then(() => createInitialData())
  .then(() => {
    const application = app.initialize();
    application.listen(process.env.SERVER_PORT || 3001);
    console.info(`The server is running on port: ${process.env.SERVER_PORT}`);
  })
  .catch((error) => {
    console.error('APP STOPPED');
    console.error(error.stack);
    return process.exit(1);
  });
