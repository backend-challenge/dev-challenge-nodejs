import TvShows from './tvshows.js';
import Actors from './actors.js';
import Auth from './auth.js';
import Movies from './movies.js';

export default {
  Auth,
  Actors,
  Movies,
  TvShows,
};
