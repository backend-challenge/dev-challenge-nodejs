import { Router } from 'express';
import mongoose from 'mongoose';
import EpisodeTvShow from '../models/EpisodeTvShow.js';
import extractJwt from '../utils/extract-jwt.js';

const router = Router();

/*
Endpoint for getting specific tv show episode info.
GET 'api/tvshows/:id'
Must send ObjectId of specific episode
 */

router.get('/episode/:id', extractJwt, (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      code: 'invalid_id',
      message: 'Invalid Id',
    });
  }
  return EpisodeTvShow.findById(req.params.id)
    .populate('director')
    .then((episodeFound) => {
      if (!episodeFound) {
        return res.status(400).json({
          code: 'no_results',
          message: 'No Resultrs',
        });
      }
      return res.status(200).json({
        episodeFound,
      });
    })
    .catch((err) => {
      console.error(`Get TvEpisode - ERROR ${err.message}`);
    });
});

export default router;
