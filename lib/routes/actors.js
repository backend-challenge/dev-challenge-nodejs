import { Router } from 'express';
import Actor from '../models/Actor.js';
import extractJwt from '../utils/extract-jwt.js';

const router = Router();

/*
Endpoint for creating actors.
POST 'api/actors/create'
Obligatory fields:
- firstName: String
- lastName: String
- age: Number
 */

router.post('/create', extractJwt, (req, res) => {
  if (!req.body) {
    return res.status(400).json({
      code: 'missing_body',
      message: 'Missing Body',
    });
  }

  if (!req.body.firstName || !req.body.lastName || !req.body.age) {
    return res.status(400).json({
      code: 'missing_parameters',
      message: 'Missing Parameters',
    });
  }
  const newActor = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    age: req.body.age,
  };

  return Actor.create(newActor)
    .then(() => res.status(200).json({
      code: 200,
      message: 'Actor Created',
    }))
    .catch((err) => {
      console.error(`Create actor - ERROR ${err.message}`);
    });
});

export default router;
