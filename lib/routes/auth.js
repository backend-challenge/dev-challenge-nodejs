import { Router } from 'express';
import jwt from 'jsonwebtoken';

const { sign } = jwt;

const router = Router();

function createToken(expiresIn, issuer) {
  const token = sign(
    { data: 'login_token' },
    process.env.JWT_SECRET,
    {
      expiresIn,
      issuer,
    },
  );
  return token;
}

function createRefreshToken(expiresIn, issuer) {
  const refreshtoken = sign(
    { data: 'refresh_token' },
    process.env.REFRESH_TOKEN_SECRET,
    {
      expiresIn,
      issuer,
    },
  );
  return refreshtoken;
}

/*
Endpoint for creating access token.
POST 'api/auth/login'
 */

router.post('/login', (req, res) => {
  const token = createToken('30m', process.env.JWT_ISSUER);
  const refreshToken = createRefreshToken('1d', process.env.JWT_ISSUER);

  res.cookie('jwt', refreshToken, {
    httpOnly: true,
    sameSite: 'None',
    secure: false,
    maxAge: 24 * 60 * 60 * 1000,
  });
  return res.status(200).json({ token });
});

/*
Endpoint for refreshing access token.
POST 'api/auth/refresh'
Must use jwt in cookies
 */

router.post('/refresh', (req, res) => {
  if (!req.cookies.jwt) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  const refreshToken = req.cookies.jwt;

  jwt.verify(
    refreshToken,
    process.env.REFRESH_TOKEN_SECRET,
    (err) => {
      if (err) {
        return res.status(400).json({ message: 'Invalid Refresh Token' });
      }
      const token = createToken('10m', process.env.JWT_ISSUER);
      return res.json({ token });
    },
  );

  return null;
});

export default router;
