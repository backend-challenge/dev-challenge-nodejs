import { Router } from 'express';
import extractJwt from '../utils/extract-jwt.js';
import Movie from '../models/Movie.js';

const sortData = ['asc', 'desc'];
const router = Router();

function parseFilters(req, res, next) {
  if (sortData.includes(req.query.sort)) {
    req.sort = req.query.sort;
  } else {
    req.sort = 'asc';
    req.sort = req.query.sort;
  }

  if (req.query.filter) {
    req.filter = { runningTime: req.query.filter };
  } else {
    req.filter = {};
  }
  return next();
}

/*
Endpoint for getting movies info.
GET 'api/movies/'
Optional query params:
- sort: Used to sorting asc or desc movies release year (e.g: sort: desc)
- filter: Used to filter movies by running time  (e.g: filter: 136)
 */

router.get('/', extractJwt, parseFilters, (req, res) => Movie.find(req.filter)
  .populate('director actors')
  .sort({ releaseYear: req.sort })
  .then((moviesFound) => {
    if (moviesFound.length === 0) {
      return res.status(200).json({ results: 'No Movies Found' });
    }
    return res.status(200).json(moviesFound);
  }))
  .catch((err) => {
    console.error(`Get Movies - ERROR ${err.message}`);
  });

export default router;
