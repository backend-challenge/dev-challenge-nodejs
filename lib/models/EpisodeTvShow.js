import { model, Schema } from 'mongoose';

const episodeTvShowSchema = new Schema({
  title: {
    type: String,
  },
  director: {
    type: Schema.Types.ObjectId,
    ref: 'Director',
    index: true,
  },
}, {
  timestamps: true,
  versionKey: false,
});

export default model('EpisodeTvShow', episodeTvShowSchema);
