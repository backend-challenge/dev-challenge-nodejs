import { model, Schema } from 'mongoose';

const movieSchema = new Schema({
  title: {
    type: String,
  },
  actors: [{
    type: Schema.Types.ObjectId,
    ref: 'Actor',
    index: true,
  }],
  director: {
    type: Schema.Types.ObjectId,
    ref: 'Director',
    index: true,
  },
  releaseYear: {
    type: Number,
  },
  runningTime: {
    type: Number,
  },
  genre: {
    type: String,
    enum: ['Action', 'Drama', 'Comedy', 'Thriller', 'Documentary'],
  },
}, {
  timestamps: true,
  versionKey: false,
});

export default model('Movie', movieSchema);
