import { model, Schema } from 'mongoose';

const seasonTvShowSchema = new Schema({
  episodeCount: {
    type: Number,
  },
  releaseYear: {
    type: Number,
  },
  episodes: [{
    type: Schema.Types.ObjectId,
    ref: 'EpisodeTvShow',
    index: true,
  }],
}, {
  timestamps: true,
  versionKey: false,
});

export default model('SeasonTvShow', seasonTvShowSchema);
