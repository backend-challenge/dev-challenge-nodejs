import { model, Schema } from 'mongoose';

const actorSchema = new Schema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  age: {
    type: Number,
  },
}, {
  timestamps: true,
  versionKey: false,
});

export default model('Actor', actorSchema);
