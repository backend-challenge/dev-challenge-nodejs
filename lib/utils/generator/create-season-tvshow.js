import SeasonTvShow from '../../models/SeasonTvShow.js';
import EpisodeTvShow from '../../models/EpisodeTvShow.js';

function createSeasonTv() {
  const newSeason = new SeasonTvShow({
    episodeCount: 10,
    releaseYear: 2022,
  });
  return SeasonTvShow.countDocuments()
    .then((count) => {
      if (count === 0) {
        return EpisodeTvShow.findOne({ title: 'Pilot' })
          .then((episodeFound) => {
            newSeason.episodes.push(episodeFound);
            return newSeason.save()
              .then(() => {
                console.info('CREATE SEASON SUCCESSFULLY');
              })
              .catch((err) => {
                console.error(`CREATE SEASON - ERROR ${err.message}`);
              });
          });
      }
      console.info('CREATE SEASON - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE SEASON COUNT - ERROR ${err.message}`);
    });
}

export default createSeasonTv;
