import Actor from '../../models/Actor.js';
import Director from '../../models/Director.js';
import Movie from '../../models/Movie.js';

function findActors() {
  return Actor.find({});
}

function findDirectors() {
  return Director.find({});
}

function createMovies() {
  const newMovies = [{
    title: 'Shutter Island',
    releaseYear: 2010,
    runningTime: 139,
    genre: 'Thriller',
  },
  {
    title: 'Shutter Island 2: Electric Boogaloo',
    releaseYear: 2015,
    runningTime: 150,
    genre: 'Drama',
  },
  {
    title: 'Shutter Island 3: The Revenge',
    releaseYear: 2019,
    runningTime: 150,
    genre: 'Comedy',
  }];
  return Movie.countDocuments()
    .then((count) => {
      if (count === 0) {
        return Promise.all([
          findActors(),
          findDirectors(),
        ])
          .then((response) => {
            const actorsFound = response[0];
            const directorsFound = response[1].shift();

            newMovies.map((movie) => {
              const completeMovie = movie;
              completeMovie.actors = actorsFound;
              completeMovie.director = directorsFound;
              return completeMovie;
            });
            return Movie.insertMany(newMovies)
              .then(() => {
                console.info('CREATE MOVIES SUCCESSFULLY');
              })
              .catch((err) => {
                console.error(`CREATE MOVIES - ERROR ${err.message}`);
              });
          });
      }
      console.info('CREATE MOVIES - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE MOVIES COUNT - ERROR ${err.message}`);
    });
}

export default createMovies;
