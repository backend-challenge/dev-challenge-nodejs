import createActor from './create-actor.js';
import createDirector from './create-director.js';
import createEpisodeTv from './create-episode-tvshow.js';
import createMovies from './create-movies.js';
import createSeasonTv from './create-season-tvshow.js';
import createTvShow from './create-tvshow.js';

export default function createInitialData() {
  return createActor()
    .then(() => createDirector())
    .then(() => createMovies())
    .then(() => createEpisodeTv())
    .then(() => createSeasonTv())
    .then(() => createTvShow())
    .catch((err) => {
      console.error(`GENERATOR - Error ${err.message}`);
    });
}
