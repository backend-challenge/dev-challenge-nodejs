import Actor from '../../models/Actor.js';

function createActor() {
  const newActors = [{
    firstName: 'Leonardo',
    lastName: 'DiCaprio',
    age: 47,
  }, {
    firstName: 'Mark',
    lastName: 'Ruffalo',
    age: 54,
  }];
  return Actor.countDocuments()
    .then((count) => {
      if (count === 0) {
        return Actor.insertMany(newActors)
          .then(() => {
            console.info('CREATE ACTOR SUCCESSFULLY');
          })
          .catch((err) => {
            console.error(`CREATE ACTOR - ERROR ${err.message}`);
          });
      }
      console.info('CREATE ACTOR - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE ACTOR COUNT - ERROR ${err.message}`);
    });
}

export default createActor;
