import Director from '../../models/Director.js';
import EpisodeTvShow from '../../models/EpisodeTvShow.js';

function createEpisodeTv() {
  const newTvEpisode = new EpisodeTvShow({
    title: 'Pilot',
  });
  return EpisodeTvShow.countDocuments({ title: 'Pilot' })
    .then((count) => {
      if (count === 0) {
        return Director.findOne({ firstName: 'Martin' })
          .then((directorFound) => {
            newTvEpisode.director = directorFound;
            return newTvEpisode.save()
              .then(() => {
                console.info('CREATE EPISODE SUCCESSFULLY');
              })
              .catch((err) => {
                console.error(`CREATE EPISODE - ERROR ${err.message}`);
              });
          });
      }
      console.info('CREATE EPISODE - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE EPISODE COUNT - ERROR ${err.message}`);
    });
}

export default createEpisodeTv;
