import Director from '../../models/Director.js';
import SeasonTvShow from '../../models/SeasonTvShow.js';
import Actor from '../../models/Actor.js';
import TvShow from '../../models/TvShow.js';

function findActors() {
  return Actor.find();
}
function findSeasonTv() {
  return SeasonTvShow.find();
}

function findDirector() {
  return Director.findOne({ firstName: 'Martin' });
}

function createTvShow() {
  const newTvShow = new TvShow({
    title: 'Killers of the flower moon',
    releaseYear: 2022,
    genre: 'Thriller',
  });
  return TvShow.countDocuments()
    .then((count) => {
      if (count === 0) {
        return Promise.all([
          findActors(),
          findSeasonTv(),
          findDirector(),
        ])
          .then((response) => {
            const actors = response[0];
            const tvSeason = response[1];
            const director = response[2];

            newTvShow.actors = actors;
            newTvShow.director = director;

            newTvShow.seasons = tvSeason;
            return newTvShow.save()
              .then(() => {
                console.info('CREATE TVSHOW SUCCESSFULLY');
              })
              .catch((err) => {
                console.error(`CREATE TVSHOW - ERROR ${err.message}`);
              });
          });
      }
      console.info('CREATE TVSHOW - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE TVSHOW COUNT - ERROR ${err.message}`);
    });
}

export default createTvShow;
