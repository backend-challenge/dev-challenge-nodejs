import Director from '../../models/Director.js';

function createDirector() {
  const newDirector = new Director({
    firstName: 'Martin',
    lastName: 'Scorsese',
    age: 79,
  });
  return Director.countDocuments({ firstName: 'Martin', lastName: 'Scorsese' })
    .then((count) => {
      if (count === 0) {
        return newDirector.save()
          .then(() => {
            console.info('CREATE DIRECTOR SUCCESSFULLY');
          })
          .catch((err) => {
            console.error(`CREATE DIRECTOR - ERROR ${err.message}`);
          });
      }
      console.info('CREATE DIRECTOR - ALREADY EXISTS');
      return null;
    })
    .catch((err) => {
      console.error(`CREATE DIRECTOR COUNT - ERROR ${err.message}`);
    });
}

export default createDirector;
