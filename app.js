import express, { json, urlencoded } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';
import * as dotenv from 'dotenv';
import cookieparser from 'cookie-parser';
import routes from './lib/routes/index.js';

const app = express();
dotenv.config();

export function connectMongoose() {
  mongoose.Promise = Promise;
  return mongoose.connect(`${process.env.MONGO_DB}`, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  });
}

export function initialize() {
  app.use(cors());
  app.use(cookieparser());
  app.use(morgan('dev'));
  app.use(json());
  app.use(urlencoded({ extended: true }));

  Object.keys(routes).forEach((key) => {
    app.use(`/api/${key}`, routes[key]);
  });

  return app;
}
