# BackEnd Nodejs Challenge



## Getting started
In order to start this project you need to create a `.env` file using `.env.dist` as a template for it. Simply copy `.env.dist` and change its name.

When the service first starts it'll create a basic data structure using mongoose.

Each endpoint is documented inside the code. Every endpoint requires a token in order to work except those used to generate it and refresh it.

## Technologies used

This project uses `express` for endpoint creation, `mongoose` as an ORM and MongoDb as its database and `eslint` as a way to enforce clean code practices.


## How to run

In order to run this project we first need to install all our dependencies using `npm install` on our project folder.
Then we simply need to run `npm start` or `npm run dev` in case we want to use `nodemon`.